import mysql.connector
import scrapper

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="gzgz",
    database="weatherdata"
)

my_cursor = mydb.cursor()
# my_cursor.execute("CREATE TABLE weather (City VARCHAR(255) PRIMARY Key, Date VARCHAR(255), MaxTemp VARCHAR(255),
# MinTemp VARCHAR(255), Wind VARCHAR(255), Humidity VARCHAR(255), Precipitation VARCHAR(255), Sunrise VARCHAR(255),
# Sunset VARCHAR(255))")
sql_command = "INSERT INTO weather (City, Date, MaxTemp, MinTemp, Wind, Humidity, Precipitation, Sunrise, " \
              "Sunset) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s) "
# data = tuple(scrapper.tunis.)
for day in scrapper.cities_data:
    my_cursor.execute(sql_command, tuple(day.values()))
mydb.commit()
