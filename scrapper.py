from bs4 import BeautifulSoup
import requests

cities_data = []


def get_city_name(url):
    return url[url.rfind("/", 0, url.rfind("/")) + 1:url.rfind("/")]


def fill_list(url):
    page = requests.get(url)
    soup = BeautifulSoup(page.content, "html.parser")
    columns = soup.find("tbody")
    lines = columns.find_all("tr")
    for line in lines:
        date = str(line.find("th"))
        temp = str(line.find_all("td")[1])
        wind = str(line.find_all("td")[4])
        humidity = str(line.find_all("td")[6])
        precipitation = str(line.find_all("td")[7])
        sunrise = str(line.find_all("td")[10])
        sunset = str(line.find_all("td")[11])
        cities_data.append({
            "City": get_city_name(url),
            "Day": date[len(date) - 12:len(date) - 5],
            "Max Temperature": temp[4:temp.index("/")] + "°C",
            "Min Temperature": temp[temp.index("/") + 2:temp.index("°")],
            "Wind": wind[wind.index(">") + 1:wind.index("/") + 2],
            "Humidity": humidity[humidity.index(">") + 1:wind.index("/") - 1],
            "Precipitation": precipitation[precipitation.index(">") + 1:precipitation.index("/") - 1],
            "Sunrise": sunrise[sunrise.index(">") + 1:sunrise.index("/") - 1],
            "sunset": sunset[sunrise.index(">") + 1:sunset.index("/") - 1]
        })


fill_list("https://www.timeanddate.com/weather/tunisia/tunis/ext")
fill_list("https://www.timeanddate.com/weather/tunisia/sousse/ext")
